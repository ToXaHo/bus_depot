<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Driver;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Drivers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Driver', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            [
                    'class' => 'yii\grid\DataColumn',
                    'header' => 'Age',
                    'value' => function ($data) {
                        return Driver::getAgeFromDateOfBirth($data->date_of_birth);
                    }
            ],
            'date_of_birth',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
