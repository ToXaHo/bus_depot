<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DriverBusRelationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Driver Bus Relations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-bus-relations-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Driver Bus Relations', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'driver_id',
            'bus_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
