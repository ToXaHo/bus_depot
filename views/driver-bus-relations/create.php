<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DriverBusRelations */

$this->title = 'Create Driver Bus Relations';
$this->params['breadcrumbs'][] = ['label' => 'Driver Bus Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-bus-relations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
