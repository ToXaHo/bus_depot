<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DriverBusRelations */

$this->title = 'Update Driver Bus Relations: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Driver Bus Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="driver-bus-relations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
