<?php

namespace app\controllers;

use app\helpers\GoogleMapsAPI;
use app\models\Driver;
use Yii;
use app\models\DriverBusRelations;
use app\models\DriverBusRelationsSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DriverBusRelationsController implements the CRUD actions for DriverBusRelations model.
 */
class DriverBusRelationsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DriverBusRelations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverBusRelationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DriverBusRelations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DriverBusRelations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DriverBusRelations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DriverBusRelations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DriverBusRelations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DriverBusRelations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DriverBusRelations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DriverBusRelations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Функционал расчета времени прохождения.
     *
     * По моей логике (возможно она не правильная), водитель который имеет доступ к самым быстрым автобусам будет всегда на первом месте.
     * Нужно получить список самых быстрых автобусов у которых есть водитель и сгруппировать их.
     *
     * @param $from
     * @param $to
     * @param null $driverId
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionTravelTime($from, $to, $driverId = null)
    {
        $distance = GoogleMapsAPI::getTravelDistance($from, $to);

        if ($distance === null || $distance === 0) {
            throw new HttpException(500, 'Расстояние между городами не смогло рассчитаться');
        }

        $query = DriverBusRelations::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 10]);

        $relations = DriverBusRelations::find()
            ->joinWith('bus')
            ->joinWith('driver')
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('buses.avg_speed DESC')
            ->groupBy('drivers.id');

        if ($driverId !== null) {
            $relations->where('drivers.id = ' . $driverId);
        }

        $relations = $relations->asArray()->all();

        $data = [];

        foreach ($relations as $relation) {
            $data[] = [
                'id' => $relation['driver']['id'],
                'name' => $relation['driver']['fio'],
                'birth_date' => $relation['driver']['date_of_birth'],
                'age' => Driver::getAgeFromDateOfBirth($relation['driver']['date_of_birth']),
                'travel_time' => $distance / ($relation['bus']['avg_speed'] * 8)
            ];
        }

        return $this->asJson($data);
    }
}
