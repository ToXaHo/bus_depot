<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drivers".
 *
 * @property int $id
 * @property string $fio
 * @property string $date_of_birth
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'date_of_birth'], 'required'],
            [['date_of_birth'], 'safe'],
            [['fio'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'date_of_birth' => 'Date Of Birth',
        ];
    }

    public function getBuses()
    {
        return $this->hasMany(DriverBusRelations::className(), ['driver_id' => 'id'])->joinWith('bus');
    }

    /**
     * Получение возраста водителя
     *
     * @param $dateOfBirth
     * @return false|int|mixed|string
     */
    public static function getAgeFromDateOfBirth($dateOfBirth)
    {
        $birthDate = explode("-", $dateOfBirth);

        return (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
    }
}
