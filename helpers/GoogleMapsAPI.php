<?php

namespace app\helpers;

class GoogleMapsAPI
{
    const API_KEY = 'AIzaSyBJPLOcJ3w4Z3THIsLGzXABDNsobTT6_us';

    public static function getTravelDistance($from, $to)
    {
        try {
            $json = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($from) . "&destinations=" . urlencode($to) . "&key=" . self::API_KEY);
            $json = json_decode($json, true);

            return $json['rows'][0]['elements'][0]['distance']['value'] / 1000;
        } catch (\Exception $exception) {
            return null;
        }
    }
}