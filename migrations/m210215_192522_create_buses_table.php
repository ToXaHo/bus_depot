<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%buses}}`.
 */
class m210215_192522_create_buses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%buses}}', [
            'id' => $this->primaryKey(),
            'name' => \yii\db\Schema::TYPE_STRING . ' NOT NULL',
            'avg_speed' => \yii\db\Schema::TYPE_INTEGER . ' NOT NULL'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%buses}}');
    }
}
