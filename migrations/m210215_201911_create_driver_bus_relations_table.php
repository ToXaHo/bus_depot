<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%driver_bus_relations}}`.
 */
class m210215_201911_create_driver_bus_relations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%driver_bus_relations}}', [
            'id' => $this->primaryKey(),
            'driver_id' => $this->integer()->notNull(),
            'bus_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-driver_bus-driver_id',
            'driver_bus_relations',
            'driver_id',
            'drivers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-driver_bus-bus_id',
            'driver_bus_relations',
            'bus_id',
            'buses',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-driver_bus-driver_id',
            'driver_id'
        );

        $this->dropForeignKey(
            'fk-driver_bus-bus_id',
            'bus_id'
        );

        $this->dropTable('{{%driver_bus_relations}}');
    }
}
